﻿using MOTChecker.Common.Models;
using MOTChecker.Service;
using System;
using System.Linq;
using System.Net.Http;
using Unity.ServiceLocation;

namespace MOTChecker
{
    internal class Program
    {
        private static void Main()
        {
            // Setup (we're using a service locator here as we cannot DI into static main)
            var serviceLocator = new UnityServiceLocator(UnityConfig.Container);
            var motCheckingService = serviceLocator.GetInstance<IMotCheckingService>();

            RunProgramLoop(motCheckingService);
        }

        private static void RunProgramLoop(IMotCheckingService motCheckingService)
        {
            while (true)
            {
                VehicleDetails vehicleDetails;

                Console.WriteLine("\nPlease enter the vehicle registration:");
                string registration = Console.ReadLine();

                try
                {
                    vehicleDetails = motCheckingService.GetMotDetailsAsync(registration).GetAwaiter().GetResult();
                }
                catch (HttpRequestException)
                {
                    Console.WriteLine("\nA network error occured, please try again.");
                    continue;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"\n{e.Message}");
                    continue;
                }

                PrintVehicleDetails(vehicleDetails);

                Console.WriteLine("\nWould you like to check another vehicle? (y/n)");
                string userInput = Console.ReadLine();

                if (userInput == "n")
                    break;
            }
        }

        private static void PrintVehicleDetails(VehicleDetails vehicleDetails)
        {
            Console.WriteLine("\n\n============================");
            Console.WriteLine($"Make: {vehicleDetails.Make}");
            Console.WriteLine($"Model: {vehicleDetails.Model}");
            Console.WriteLine($"Colour: {vehicleDetails.PrimaryColour}");

            if (vehicleDetails.MotTests != null)
            {
                Console.WriteLine($"MOT Expiry Date: {vehicleDetails.MotTests.First().ExpiryDate.ToShortDateString()}");
                Console.WriteLine($"Mileage at Last MOT: {vehicleDetails.MotTests.First().OdometerValue:n0}");
            }
            else
            {
                Console.WriteLine("No MOT data available.");
            }

            Console.WriteLine("============================\n");
        }
    }
}
