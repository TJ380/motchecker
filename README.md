# Vehicle MOT Checker
A console application which prompts the user for a registration number so that they can check when to renew their MOT. 

## Running the Program

Clone the repo by running the command:

`git clone https://bitbucket.org/TJ380/motchecker/src/master/`

Or download the solution by using this link https://bitbucket.org/TJ380/motchecker/get/4ee41c43552b.zip

Either run `MOTChecker.exe` in the `/bin` folder or compile the source directly. A package restore is necessary, which Visual Studio should do automatically on the second build attempt.

## Solution Structure
The solution is split into three projects:
* MOTChecker.Common
	* Contains the domain models: Vehicle Test and MotDetails. 
* MOTChecker.Service
	*  Resonsible for hitting the .gov.uk API endpoint and deserializing its responses into domain models.
* MOTChecker.CLI
	* The UI, receives user input and displays the contents of the domain model afterwards.

The CLI project can easily be replaced with an MVC, UWP or other application, by referencing the MOTChecker.Service project.

The solution targets .NET Framework 4.5.2

## Packages Used
* Microsoft.AspNet.WebApi.Client
	* Provides ReadAsAsync extension method to HttpResponseMessage.Content for deserializing JSON to a strongly-typed model.
* Newtonsoft.Json
	* Dependency of the above package.
* Unity and its sub-packages
	* Used for service location of MOTChecker.Service

## Acceptance Criteria
The user is prompted for a vehicle registration.
If a valid registration is found, the make, model, colour, current MOT expiry date and mileage at the last MOT are displayed.

![Screen capture of the running program](https://www.dropbox.com/s/is66tiz043ko52b/motchecker_capture.JPG?dl=1)


