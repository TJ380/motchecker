﻿using System.Threading.Tasks;
using MOTChecker.Common.Models;

namespace MOTChecker.Service
{
    public interface IMotCheckingService
    {
        Task<VehicleDetails> GetMotDetailsAsync(string vehicleRegistration);
    }
}