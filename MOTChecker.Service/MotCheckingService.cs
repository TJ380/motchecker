﻿using MOTChecker.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MOTChecker.Service
{
    public class MotCheckingService : IMotCheckingService
    {
        private static readonly HttpClient HttpClient = new HttpClient();
        private const string ApiEndpoint = "https://beta.check-mot.service.gov.uk/trade/vehicles/mot-tests";
        private const string ApiKey = "fZi8YcjrZN1cGkQeZP7Uaa4rTxua8HovaswPuIno";

        public async Task<VehicleDetails> GetMotDetailsAsync(string vehicleRegistration)
        {
            var motDetails = await CreateRequestAsync(vehicleRegistration.Replace(" ", ""));

            return motDetails;
        }

        private static async Task<VehicleDetails> CreateRequestAsync(string vehicleRegistration)
        {
            var vehicleDetails = new List<VehicleDetails>();

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{ApiEndpoint}?registration={vehicleRegistration}"),
                Headers =
                    {
                        {"x-api-key", ApiKey}
                    }
            };

            var response = await HttpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                vehicleDetails = await response.Content.ReadAsAsync<List<VehicleDetails>>();
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                throw new Exception("Vehicle registration not found.");
            }

            return vehicleDetails.FirstOrDefault();
        }
    }
}