﻿using System;

namespace MOTChecker.Common.Models
{
    public class MotTest
    {
        public string CompletedDate { get; set; }
        public string TestResult { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal OdometerValue { get; set; }
        public string OdometerUnit { get; set; }
        public string MotTestNumber { get; set; }
    }
}
